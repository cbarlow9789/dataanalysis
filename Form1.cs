﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace PolarData
{
    public partial class Form1 : Form
    {
        Stats Stats = new Stats();
        Stats ZoomStats = new Stats();
        Stats intervalAverages = new Stats();
        SMODE smode = new SMODE();
        Conversions Conversions = new Conversions();
        public string[] lines;
        public string[] data;
        public string HRdata;
        private DateTime starttimeformatted;
        private int intervaltime;
        private double timeSeconds;
        private TimeSpan length;

        GraphPane graph;
        PointPairList graphhr;
        PointPairList speedgraph;
        PointPairList powergraph;
        PointPairList altitudegraph;
        PointPairList cadencegraph;
        LineItem curvehr;
        LineItem curvespeed;
        LineItem curvepower;
        LineItem curvealtitude;
        LineItem curvecadence;
        private string unitDistance;
        private string unitSpeed;
        private string unitDistance2;
        private string unitTemp;

        public int rowAvailable;

        private bool converted = false;

        public int rowcount;

        public List<double> IntervalList = new List<double>();
        public List<double> IntervalList2 = new List<double>();

        public Form1()
        {
            InitializeComponent();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {

            // Show the dialog and get result.
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK) // Test result.
            {
                string s = openFileDialog1.FileName;


                openFile(s);

            }
   
            FetchParams();
            smodeCheck();
            HRData();
        }

        private void openFile(string s)
        {
            string text;
            string text2;
            using (StreamReader reader = new StreamReader(s))
            {
                while (!reader.EndOfStream)
                {
                    text = reader.ReadToEnd();
                    text2 = text;

                    text = text.Replace("]", " ");
                    lines = text.Split('[');

                    if (text2.Contains("[HRData]")){
                        data = text2.Split(new[] { "[HRData]" }, StringSplitOptions.None);
                        HRdata = data[1];
                    }
                }
            }
        }
        private void FetchParams()
        {
            string param = lines[1].ToString();     //Save params section as strint
            string[] paramvalues = param.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);      //Save each line as a new parameter
            foreach (string s in paramvalues)
            {
                if (s.Contains("Date")){        //If the line contains 'date'
                    string[] date = s.Split('=');
                    DateTime startdate = FormatDate(date[1]);
                    lblDate.Text = date[0] + ": " + startdate.ToString("dd/MM/yyyy");       //Display date value on form
                }
                if (s.Contains("Length"))       //If the line contains 'length'
                {
                    string[] lengths = s.Split('=');
                    string[] lengthNoPoint = lengths[1].Split('.');
                 
                    length = TimeSpan.Parse(lengthNoPoint[0]);
                    timeSeconds = length.TotalSeconds;
                    lblLength.Text = "Length: " + length;       //Display length value on form

                }
                if (s.Contains("Interval"))     //If the line contains 'interval'
                {
                    string[] interval = s.Split('=');
                    lblInterval.Text ="Interval: " + interval[1] + " Seconds";      //Display interval value on form
                    intervaltime = Convert.ToInt32(interval[1]);
                    Stats.Interval = intervaltime;
                }

                if (s.Contains("StartTime"))        //If the line contains 'starttime'
                {
                    string[] start = s.Split('=');
                    string[] startnopoint = start[1].Split('.');
                    starttimeformatted = Convert.ToDateTime(startnopoint[0]);
                    lblStartTime.Text = "Start Time: " + starttimeformatted.ToString();     //Display start time value on form
                }

                if (s.Contains("SMode"))        //If the line contains 'smode'
                {
                    string[] smodeValue = s.Split('=');
                    smode.smode = smodeValue[1];
                    smode.smodeBits(smodeValue[1]);
                }
                Debug.WriteLine(s);
            }
        }

        private DateTime FormatDate(string date)        //Format any date values passed into function
        {
            var newDate = DateTime.ParseExact(date, "yyyyMMdd", CultureInfo.InvariantCulture);      
            return newDate;
        }

        public void HRData()
        {
            dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();
            //set x and y coordinates
            graph = zedGraphControl1.GraphPane;
            graph.CurveList.Clear();        //Clear graph

            graph.Title.Text = "Cycle Analysis Graph";      //Set titles and axis
            graph.XAxis.Title.Text = "Time";
            graph.YAxis.Title.Text = "Data";

            graphhr = new PointPairList();
            speedgraph = new PointPairList();
            powergraph = new PointPairList();
            altitudegraph = new PointPairList();
            cadencegraph = new PointPairList();

            string data = HRdata.ToString();
            string[] hrdata = data.Split('\n');     //Split each Heart Rate data every new line

            rowcount = 0;


            int ii = 0;
            for (int i = 1; i < hrdata.Length; i++)
            {
                string s = hrdata[i];
                s = s.Trim('\r');
                Stats.TotalRows = hrdata.Length - 1;
                string[] values1 = (s.Split('\t'));     //Split each heart rate value after a tab

                starttimeformatted = starttimeformatted.AddSeconds(intervaltime);
                this.dataGridView1.Rows.Add();
                this.dataGridView1.Rows[ii].Cells["time"].Value = starttimeformatted.ToString("HH:mm:ss"); //Time
                this.dataGridView1.Rows[ii].Cells["heartrate"].Value = values1[0];        //Heart Rate

                if (smode.units == true)
                {
                    dataGridView1.Columns[3].HeaderText = "Speed (mph)";
                    dataGridView1.Columns[5].HeaderText = "Altitude (ft)";
                }

                
                double speed = Convert.ToDouble(values1[1]) / 10;       
                this.dataGridView1.Rows[ii].Cells["speed"].Value = speed;     //Speed
                this.dataGridView1.Rows[ii].Cells["cadence"].Value = values1[2];        //Cadence
                this.dataGridView1.Rows[ii].Cells["altitude"].Value = values1[3];        //Altitude
                this.dataGridView1.Rows[ii].Cells["power"].Value = values1[4];        //Power
                if (values1.Length > 5)
                {
                    this.dataGridView1.Rows[ii].Cells["powerbalanceindex"].Value = values1[5];        //Power Balance and Pedaling index
                }

                double time = starttimeformatted.ToOADate();

                lblEntries.Text = "Entries: " + Stats.RowCount.ToString();

                Stats.HR = Convert.ToDouble(values1[0]);            //Pass all values into Stats class to calcuate averages, max, min.
                Stats.maxHR(Stats.HR);                              //
                Stats.minHR(Stats.HR);                              //  
                Stats.averageHR(Stats.HR);                          //
                                                                    //
                Stats.Power = Convert.ToInt32(values1[4]);          //
                Stats.maxPower(Stats.Power);                        //
                Stats.TotalPower = Stats.TotalPower + Stats.Power;

                rowAvailable = 0;
                if (Stats.Power >= (Stats.FunctionalThresholdPower - (Stats.FunctionalThresholdPower * 0.10)))
                {
                    rowAvailable++;
                    this.dataGridView1.Rows[rowcount].Cells[rowAvailable].Value = "Yes";
                }
                else
                {
                    rowAvailable++;
                    this.dataGridView1.Rows[rowcount].Cells[rowAvailable].Value = "No";
                }
                rowcount++;

                if (Stats.Power > 0)
                {
                    Stats.RowCount = Stats.RowCount + 1;
                }


                Stats.Altitude = Convert.ToInt32(values1[3]);       //
                Stats.maxAltitude(Stats.Altitude);                  //
                Stats.averageAltitude(Stats.Altitude);              //  
                                                                    //
                Stats.Speed = Convert.ToDouble(speed);              //
                Stats.maxSpeed(Stats.Speed);                        //
                Stats.averageSpeed(Stats.Speed);                    //

                if (values1.Length > 5)
                {
                    Stats.PowerBalanceValue = Convert.ToInt32(values1[5]);
                    Stats.LRPowerBalancePedalIndex(Stats.PowerBalanceValue);
                }

                this.dataGridView1.Rows[ii].Cells["leftlegpower"].Value = Stats.LeftPower;
                this.dataGridView1.Rows[ii].Cells["rightlegpower"].Value = Stats.RightPower;
                this.dataGridView1.Rows[ii].Cells["pedalindex"].Value = Stats.PedalIndex;




                graphhr.Add(time, Convert.ToDouble(values1[0]));              //Plot all points on the graph
                speedgraph.Add(time, Convert.ToDouble(speed));
                cadencegraph.Add(time, Convert.ToDouble(values1[2]));
                altitudegraph.Add(time, Convert.ToDouble(values1[3]));
                powergraph.Add(time, Convert.ToDouble(values1[4]));

                ii++;           //Next row

            }

            lblMaxHR.Text = "Max HR: " + Stats.MaxHR.ToString() + " bpm";           //Display statistics in labels on form
            lblMinHR.Text = "Min HR: " + Stats.MinHR.ToString() + " bpm";
            lblMaxPower.Text = "Max Power: " + Stats.MaxPower.ToString() + " Watts";
            lblMaxAltitude.Text = "Max Altitude: " + Stats.MaxAltitude.ToString() + " " + unitDistance2;
            lblMaxSpeed.Text = "Max Speed: " + Stats.MaxSpeed.ToString() + " " + unitSpeed;
            lblAverageSpeed.Text = "Average Speed: " + Math.Round(Stats.AverageSpeed, 2).ToString() + " " + unitSpeed;


            int size = Stats.TotalRows;
            double[] powerdata = new double[size];
            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                powerdata[i] = Convert.ToDouble(dataGridView1.Rows[i].Cells["power"].Value);
            }



            Stats.normAveragePower(powerdata);
            lblAveragePower.Text = "Average Power: " + Math.Round(Stats.AveragePower, 2).ToString() + " Watts";
            


            lblAverageAltitude.Text = "Average Altitude: " + Math.Round(Stats.AverageAltitude, 2).ToString() + " " + unitDistance2;
            lblAverageHR.Text = "Average HR: " + Math.Round(Stats.AverageHR, 2).ToString() + " bpm";

            Stats.totalDistance(timeSeconds);
            lblTotalDistance.Text = "TotalDistance: " + Math.Round(Stats.TotalDistance, 2).ToString() + " " + unitDistance;

            curvehr = graph.AddCurve("Heart Rate", graphhr, Color.Blue, SymbolType.None);           //Plot graph curves
            curvespeed = graph.AddCurve("Speed", speedgraph, Color.Red, SymbolType.None);
            curvealtitude = graph.AddCurve("Altitude", altitudegraph, Color.Green, SymbolType.None);
            curvecadence = graph.AddCurve("Cadence", cadencegraph, Color.Orange, SymbolType.None);
            curvepower = graph.AddCurve("Power", powergraph, Color.Black, SymbolType.None);

            graph.XAxis.Type = AxisType.Date;

            
            Stats.normalizedPower(Stats.RollingAverage(powerdata, Stats.Interval));
            lblNormalizedPower.Text = "Normalized Power (NP): " + Math.Round(Stats.NormalizedPower, 2);

            Stats.trainingStressScore();
            lblTrainingStressScore.Text = "Training Stress Score (TSS): " + Math.Round(Stats.TrainingStressScore, 2);
            lblIntensityFactor.Text = "Intensity Factor (IF): " + Math.Round(Stats.IntensityFactor * 100, 2) + "%";
            lblFTP.Text = "FTP Value: (Default 310) : Value = " + Stats.FunctionalThresholdPower;
            txtFTP.Text = Stats.FunctionalThresholdPower.ToString();

            zedGraphControl1.AxisChange();
            zedGraphControl1.Refresh();

            Stats.resetStats();





        }

        private void smodeCheck()
        {
            if (smode.speed == true)
            {
                lblSmodeSpeed.Text = "Speed: ON";
            }

            if (smode.cadence == true)
            {
                lblSmodeCadence.Text = "Cadence: ON";
            }

            if (smode.altitude == true)
            {
                lblSmodeAltitude.Text = "Altitude: ON";
            }

            if (smode.power == true)
            {
                lblSmodePower.Text = "Power: ON";
            }

            if (smode.powerLRBalance == true)
            {
                lblSmodePowerLRBalance.Text = "Power LR Balance: ON";
            }

            if (smode.powerPedalingIndex == true)
            {
                lblSmodePowerPedalingIndex.Text = "Power Pedaling Index: ON";
            }

            if (smode.data == true)
            {
                lblSmodeData.Text = "Data: HR + Cycling Data";
            }

            if (smode.units == true)
            {
                lblSmodeUnit.Text = "Units: 'miles, mph, ft, °F'";
                unitDistance = "miles";
                unitSpeed = "mph";
                unitDistance2 = "ft";
                unitTemp = "°F";
            }else
            {
                lblSmodeUnit.Text = "Units: 'km, km/h, m, °C'";
                unitDistance = "km";
                unitSpeed = "km/h";
                unitDistance2 = "m";
                unitTemp = "°C";
            }

            if (smode.airPressure == true)
            {
                lblSmodeAirPresure.Text = "Air Pressure: ON";
            }
        }

        private void buttonConvertUnits_Click(object sender, EventArgs e)
        {
            if ((smode.units == true) && (converted == false)) {
                lblMaxSpeed.Text = "Max Speed: " + Math.Round(Conversions.ConvertMilesToKilometers(Stats.MaxSpeed), 2).ToString() + " km/h";
                lblAverageSpeed.Text = "Average Speed: " + Math.Round(Conversions.ConvertMilesToKilometers(Stats.AverageSpeed), 2).ToString() + " km/h";
                lblMaxAltitude.Text = "Max Altitude: " + Math.Round(Conversions.ConvertFeetToMeters(Stats.MaxAltitude), 2).ToString() + " m";
                lblAverageAltitude.Text = "Average Altitude: " + Math.Round(Conversions.ConvertFeetToMeters(Stats.AverageAltitude), 2).ToString() + " m";

                converted = true;
            }else if ((smode.units == false) && (converted == false))
            {
                lblMaxSpeed.Text = "Max Speed: " + Math.Round(Conversions.ConvertKilometersToMiles(Stats.MaxSpeed), 2).ToString() + " mph";
                lblAverageSpeed.Text = "Average Speed: " + Math.Round(Conversions.ConvertKilometersToMiles(Stats.AverageSpeed), 2).ToString() + " mph";
                lblMaxAltitude.Text = "Max Altitude: " + Math.Round(Conversions.ConvertMetersToFeet(Stats.MaxAltitude), 2).ToString() + " ft";
                lblAverageAltitude.Text = "Average Altitude: " + Math.Round(Conversions.ConvertMetersToFeet(Stats.AverageAltitude), 2).ToString() + " ft";

                converted = true;
            }
            else
            {
                lblMaxSpeed.Text = "Max Speed: " + Stats.MaxSpeed.ToString() + " " + unitSpeed;
                lblAverageSpeed.Text = "Average Speed: " + Math.Round(Stats.AverageSpeed, 2).ToString() + " " + unitSpeed;
                lblMaxAltitude.Text = "Max Altitude: " + Stats.MaxAltitude.ToString() + " " + unitDistance2;
                lblAverageAltitude.Text = "Average Altitude: " + Math.Round(Stats.AverageAltitude, 2).ToString() + " " + unitDistance2;

                converted = false;
            }


        }

        private void zedGraphControl1_Load(object sender, EventArgs e)
        {

        }

        private void zedGraphControl1_ZoomEvent(ZedGraphControl sender, ZoomState oldState, ZoomState newState)
        {
            double xMin = graph.XAxis.Scale.Min;
            double yMin = graph.YAxis.Scale.Min;
            double xMax = graph.XAxis.Scale.Max;
            double yMax = graph.YAxis.Scale.Max;
            ZoomStats.TotalRows = 0;
            foreach (PointPair pt in graphhr)
            {
                if (pt.X >= xMin && pt.X <= xMax && pt.Y >= yMin && pt.Y <= yMax)
                {
                    
                    ZoomStats.maxHR(pt.Y);
                    ZoomStats.minHR(pt.Y);
                    ZoomStats.averageHR(pt.Y);
                    ZoomStats.TotalRows++;

                }
            }


            foreach (PointPair pt in speedgraph)
            {

                if (pt.X >= xMin && pt.X <= xMax && pt.Y >= yMin && pt.Y <= yMax)
                {

                    ZoomStats.maxSpeed(pt.Y);
                    ZoomStats.averageSpeed(pt.Y);

                }
            }

            foreach (PointPair pt in powergraph)
            {

                if (pt.X >= xMin && pt.X <= xMax && pt.Y >= yMin && pt.Y <= yMax)
                {

                    ZoomStats.maxPower(Convert.ToDouble(pt.Y));
                    IntervalList.Add(pt.Y);
                    if (pt.Y > 0)
                    {
                        ZoomStats.averagePower(pt.Y);
                        ZoomStats.PowerRows++;
                    }



                }
            }

            foreach (PointPair pt in altitudegraph)
            {

                if (pt.X >= xMin && pt.X <= xMax && pt.Y >= yMin && pt.Y <= yMax)
                {

                    ZoomStats.maxAltitude(Convert.ToInt32(pt.Y));
                    ZoomStats.averageAltitude(pt.Y);
                }
            }

            double[] zoompower = IntervalList.ToArray();
            Stats.normalizedPower(Stats.RollingAverage(zoompower, Stats.Interval));
            lblZoomNormalizedPower.Text = "Normalized Power (NP): " + Math.Round(Stats.NormalizedPower, 2);

            lblZoomMaxHR.Text = ("Max HR: " + ZoomStats.MaxHR);
            lblZoomMinHR.Text = ("Min HR: " + ZoomStats.MinHR);
            lblZoomAverageHR.Text = ("Average HR: " + Math.Round(ZoomStats.AverageHR, 2));

            lblZoomMaxSpeed.Text = ("Max Speed: " + ZoomStats.MaxSpeed);
            lblZoomAverageSpeed.Text = ("Average Speed: " + Math.Round(ZoomStats.AverageSpeed, 2));

            lblZoomMaxPower.Text = ("Max Power: " + ZoomStats.MaxPower);

            lblZoomAveragePower.Text = ("Average Power: " + Math.Round(ZoomStats.AveragePower, 2));

            lblZoomMaxAltitude.Text = ("Max Altitude: " + ZoomStats.MaxAltitude);
            lblZoomAverageAltitude.Text = ("Average Altitude: " + Math.Round(ZoomStats.AverageAltitude, 2));

            ZoomStats.resetStats();
            IntervalList.Clear();
        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            if (daySelectedCB.Items.Count > 0)
            {
                daySelectedCB.DataSource = null;
                daySelectedCB.Items.Clear();
                summaryLBL.Text = "";
                summaryBtn.Visible = false;
                daySelectedCB.SelectedIndex = -1;

            }
            
            smode.SelectedDate = monthCalendar1.SelectionStart.ToString("yyMMdd");
            ProcessDirectory();
        }

        public void ProcessDirectory()
        {

            BindingSource bs = new BindingSource();
            //DirectoryInfo d = new DirectoryInfo(@"F:\\YEAR3\\Software Engineering\\DataAnalysis\\August2012DataForCalandar");//Assuming Test is your Folder
            DirectoryInfo d = new DirectoryInfo(@"C:\\Users\\conor\\OneDrive\\Documents\\Uni Work\\Software Engineering\\PolarData\\August2012DataForCalandar");//Assuming Test is your Folder


            FileInfo[] Files = d.GetFiles("*" + smode.SelectedDate + "*.hrm"); //Getting Text files
            string str = "";
            List<string> fileList = new List<string>() { };

            foreach (FileInfo file in Files)
            {
                fileList.Add(file.Name);
            }

            bs.DataSource = fileList;
            daySelectedCB.DataSource = bs;
        }

        private void daySelectedCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (daySelectedCB.SelectedItem != null)
            {
                //Debug.WriteLine(daySelectedCB.SelectedItem.ToString());
                //string filePath = "F:\\YEAR3\\Software Engineering\\DataAnalysis\\August2012DataForCalandar\\";
                string filePath = "C:\\Users\\conor\\OneDrive\\Documents\\Uni Work\\Software Engineering\\PolarData\\August2012DataForCalandar\\";

                StreamReader myfilereader = new StreamReader(filePath + daySelectedCB.SelectedItem.ToString());

                string line;
                StringBuilder sb = new StringBuilder();
                while ((line = myfilereader.ReadLine()) != null)
                {

                    if (line.Contains("Length"))
                    {
                        sb.AppendLine("Length:" + line.Substring(line.IndexOf("=") + 1));//get duration

                    }
                    else if (line.Contains("StartTime"))
                    {
                        sb.AppendLine("StartTime:" + line.Substring(line.IndexOf("=") + 1));
                    }
                    else if (line.Contains("Interval"))
                    {
                        sb.AppendLine("Interval:" + line.Substring(line.IndexOf("=") + 1));
                    }
                }
                summaryLBL.Text = sb.ToString();
                summaryBtn.Visible = true;
            }

        }

        private void summaryBtn_Click(object sender, EventArgs e)
        {
            //string filePath = "F:\\YEAR3\\Software Engineering\\DataAnalysis\\August2012DataForCalandar\\";
            string filePath = "C:\\Users\\conor\\OneDrive\\Documents\\Uni Work\\Software Engineering\\PolarData\\August2012DataForCalandar\\";

            string s = filePath + daySelectedCB.SelectedItem.ToString();

            openFile(s);


            FetchParams();
            smodeCheck();
            HRData();
        }

        private void btnDetectInterval_Click(object sender, EventArgs e)
        {
            //button to calculate intervals -can only click after grid is populated
            int intervalCount = 0;

            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                double test = Convert.ToDouble(dataGridView1.Rows[i].Cells["power"].Value.ToString());

                if ((i + 1) < dataGridView1.Rows.Count - 1)
                {
                    double test1 = Convert.ToDouble(dataGridView1.Rows[i+1].Cells["power"].Value.ToString());
                    if (test >= (Stats.FunctionalThresholdPower - (Stats.FunctionalThresholdPower * 0.10)))
                    {
                        dataGridView1.Rows[i].Cells["interval"].Value = intervalCount;

                        if (test1 < (Stats.FunctionalThresholdPower - (Stats.FunctionalThresholdPower * 0.10)))
                        {
                            IntervalList.Add(intervalCount);
                            intervalCount++;
                        }
                        else
                        {

                        }
                    }

                }

            }
            BindingSource bs = new BindingSource();
            bs.DataSource = IntervalList;
            intervalListsCB.DataSource = bs;
            intervalListsCB.Visible = true;
            selectIntBtn.Visible = true;
        
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void selectIntBtn_Click(object sender, EventArgs e)
        {
            DateTime starttime = DateTime.MaxValue;
            DateTime endtime = DateTime.MinValue;

            string selectedinterval = intervalListsCB.SelectedValue.ToString();


            Form intervalForm = new IntervalForm();

            intervalForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            intervalForm.Text = "Interval " + selectedinterval;
            intervalAverages.resetStats();

            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                if (dataGridView1.Rows[i].Cells["interval"].Value.ToString() == selectedinterval)
                {
                  
                        
                    DateTime time = Convert.ToDateTime(dataGridView1.Rows[i].Cells["time"].Value.ToString());
                    if (time < starttime)
                    {
                        starttime = time;
                    }
                    if (time > endtime)
                    {
                        endtime = time;
                    }

                    if (smode.data == true)
                    {
                        intervalAverages.HR = Convert.ToDouble(dataGridView1.Rows[i].Cells["heartrate"].Value);
                        intervalAverages.averageHR(intervalAverages.HR);
                        intervalAverages.maxHR(intervalAverages.HR);
                        intervalAverages.minHR(intervalAverages.HR);
                    }
                    if (smode.speed == true)
                    {
                        intervalAverages.Speed = Convert.ToDouble(dataGridView1.Rows[i].Cells["speed"].Value);
                        intervalAverages.averageSpeed(intervalAverages.Speed);
                        intervalAverages.maxSpeed(intervalAverages.Speed);
                    }
                    if (smode.altitude == true)
                    {
                        intervalAverages.Altitude = Convert.ToDouble(dataGridView1.Rows[i].Cells["altitude"].Value);

                        intervalAverages.averageAltitude(intervalAverages.Altitude);
                        intervalAverages.maxAltitude(intervalAverages.Altitude);
                    }
                    if (smode.power == true)
                    {
                        intervalAverages.Power = Convert.ToDouble(dataGridView1.Rows[i].Cells["power"].Value);
                        IntervalList2.Add(intervalAverages.Power);

                        intervalAverages.averagePower(intervalAverages.Power);
                        intervalAverages.maxPower(intervalAverages.Power);
                    }
                    intervalAverages.TotalRows++;//increment row counter / used to calculate averages
                    intervalAverages.PowerRows++;

                }

            }

            double[] zoompower2 = IntervalList2.ToArray();
            Stats.normalizedPower(Stats.RollingAverage(zoompower2, Stats.Interval));
            IntervalList2.Clear();

            intervalForm.Show();
            System.Windows.Forms.Label MaxHR = new System.Windows.Forms.Label();
            System.Windows.Forms.Label MinHR = new System.Windows.Forms.Label();
            System.Windows.Forms.Label MaxPower = new System.Windows.Forms.Label();
            System.Windows.Forms.Label MaxSpeed = new System.Windows.Forms.Label();
            System.Windows.Forms.Label MaxAltitude = new System.Windows.Forms.Label();

            System.Windows.Forms.Label AverageHR = new System.Windows.Forms.Label();
            System.Windows.Forms.Label AverageSpeed = new System.Windows.Forms.Label();
            System.Windows.Forms.Label AveragePower = new System.Windows.Forms.Label();
            System.Windows.Forms.Label AverageAltitude = new System.Windows.Forms.Label();

            System.Windows.Forms.Label StartTime = new System.Windows.Forms.Label();
            System.Windows.Forms.Label EndTime = new System.Windows.Forms.Label();

            System.Windows.Forms.Label NormalizedPower = new System.Windows.Forms.Label();



            MaxHR.Location = new Point(10, 25);
            MinHR.Location = new Point(10, 50);
            MaxPower.Location = new Point(10, 75);
            MaxSpeed.Location = new Point(10, 100);
            MaxAltitude.Location = new Point(10, 125);

            AverageHR.Location = new Point(10, 150);
            AverageSpeed.Location = new Point(10, 175);
            AveragePower.Location = new Point(10, 200);
            AverageAltitude.Location = new Point(10, 225);

            StartTime.Location = new Point(10, 250);
            EndTime.Location = new Point(10, 275);

            NormalizedPower.Location = new Point(10, 300);


            MaxHR.Name = "lblMaxHR";
            MaxHR.Text = "Max HR: " + intervalAverages.MaxHR;

            MinHR.Name = "lblMinHR";
            MinHR.Text = "Min HR: " + intervalAverages.MinHR;

            MaxPower.Name = "lblMaxPower";
            MaxPower.Text = "Max Power: " + intervalAverages.MaxPower;

            MaxSpeed.Name = "lblMaxSpeed";
            MaxSpeed.Text = "Max Speed: " + intervalAverages.MaxSpeed;

            MaxAltitude.Name = "lblMaxAltitude";
            MaxAltitude.Text = "Max Altitude: " + intervalAverages.MaxAltitude;

            AverageHR.Name = "lblAverageHR";
            AverageHR.Text = "Average HR: " + intervalAverages.AverageHR;

            AveragePower.Name = "lblAveragePower";
            AveragePower.Text = "Average Power: " + intervalAverages.AveragePower;

            AverageSpeed.Name = "lblAverageSpeed";
            AverageSpeed.Text = "Average Speed: " + intervalAverages.AverageSpeed;

            AverageAltitude.Name = "lblAverageAltitude";
            AverageAltitude.Text = "Average Altitude: " + intervalAverages.AverageAltitude;

            StartTime.Name = "lblStarttTime";
            StartTime.Text = "Start Time: " + starttime;

            EndTime.Name = "lblEndtTime";
            EndTime.Text = "End Time: " + endtime;

            NormalizedPower.Text = "Normalized Power: " + Stats.NormalizedPower;






            MaxHR.AutoSize = true;
            MinHR.AutoSize = true;
            MaxPower.AutoSize = true;
            MaxSpeed.AutoSize = true;
            MaxAltitude.AutoSize = true;

            AverageHR.AutoSize = true;
            AveragePower.AutoSize = true;
            AverageSpeed.AutoSize = true;
            AverageAltitude.AutoSize = true;

            StartTime.AutoSize = true;
            EndTime.AutoSize = true;

            NormalizedPower.AutoSize = true;






            intervalForm.Controls.Add(MaxHR);
            intervalForm.Controls.Add(MinHR);
            intervalForm.Controls.Add(MaxPower);
            intervalForm.Controls.Add(MaxSpeed);
            intervalForm.Controls.Add(MaxAltitude);
            intervalForm.Controls.Add(AverageHR);
            intervalForm.Controls.Add(AveragePower);
            intervalForm.Controls.Add(AverageSpeed);
            intervalForm.Controls.Add(AverageAltitude);
            intervalForm.Controls.Add(StartTime);
            intervalForm.Controls.Add(EndTime);
            intervalForm.Controls.Add(NormalizedPower);



        }

        private void btnChangeFTP_Click(object sender, EventArgs e)
        {
            Stats.FunctionalThresholdPower = Convert.ToInt32(txtFTP.Text);
            ZoomStats.FunctionalThresholdPower = Convert.ToInt32(txtFTP.Text);

            lblFTP.Text = "FTP Value: (Default 310) : Value = " + Stats.FunctionalThresholdPower;
        }
    }
           
}