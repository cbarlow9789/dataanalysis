﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolarData
{
    class Conversions
    {
        public static double ConvertMilesToKilometers(double miles)     //Convert miles to kilometers
        {
            
            return miles * 1.609344;
        }

        public static double ConvertKilometersToMiles(double kilometers)        //Convert kilometers into miles
        {
           
            return kilometers * 0.621371192;
        }

        public static double ConvertFeetToMeters(double feet)           //Convert feet into meters
        {
           
            return feet * 0.3048000;
        }

        public static double ConvertMetersToFeet(double feet)           //Convert meters into feet
        {

            return feet * 3.370079;
        }
    }
}

