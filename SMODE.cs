﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolarData
{
    class SMODE
    {
        public string smode;
        public bool speed = false;
        public bool cadence = false;
        public bool altitude = false;
        public bool power = false;
        public bool powerLRBalance = false;
        public bool powerPedalingIndex = false;
        public bool data = false;
        public bool units = false;
        public bool airPressure = false;
        public string SelectedDate;

        public void smodeBits(string smodeBits)
        {
            char[] SmodeBits = smodeBits.ToCharArray();     //Converts SMODE string into character array
            if (SmodeBits[0] == 49)         //If bit is equal to character '49' (1)
            {
                speed = true;       //Set speed to true
            }

            if (SmodeBits[1] == 49)         //If bit is equal to character '49' (1)
            {
                cadence = true;     //Set cadence to true
            }

            if (SmodeBits[2] == 49)         //If bit is equal to character '49' (1)
            {
                altitude = true;        //Set altitude to true
            }

            if (SmodeBits[3] == 49)         //If bit is equal to character '49' (1)
            {
                power = true;       //Set power to true
            }

            if (SmodeBits[4] == 49)         //If bit is equal to character '49' (1)
            {
                powerLRBalance = true;      //Set Power left right balance to true
            }

            if (SmodeBits[5] == 49)         //If bit is equal to character '49' (1)
            {
                powerPedalingIndex = true;      //Set power pedaling index to true
            }

            if (SmodeBits[6] == 49)         //If bit is equal to character '49' (1)
            {
                data = true;            //Set data to true
            }

            if (SmodeBits[7] == 49)             //If bit is equal to character '49' (1)
            {
                units = true;           //Set units to true
            }
            if (SmodeBits.Length > 8)
            {
                if (SmodeBits[8] == 49)             //If bit is equal to character '49' (1)
                {
                    airPressure = true;         //Set air pressure to true
                }
            }
        }
    }
}
