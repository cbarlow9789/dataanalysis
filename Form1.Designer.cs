﻿namespace PolarData
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblStartTime = new System.Windows.Forms.Label();
            this.lblInterval = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.interval = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.heartrate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.speed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cadence = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.altitude = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.power = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.powerbalanceindex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.leftlegpower = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rightlegpower = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pedalindex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zedGraphControl1 = new ZedGraph.ZedGraphControl();
            this.lblLength = new System.Windows.Forms.Label();
            this.groupBoxSmode = new System.Windows.Forms.GroupBox();
            this.lblSmodeAirPresure = new System.Windows.Forms.Label();
            this.lblSmodeUnit = new System.Windows.Forms.Label();
            this.lblSmodeData = new System.Windows.Forms.Label();
            this.lblSmodePowerPedalingIndex = new System.Windows.Forms.Label();
            this.lblSmodePowerLRBalance = new System.Windows.Forms.Label();
            this.lblSmodePower = new System.Windows.Forms.Label();
            this.lblSmodeAltitude = new System.Windows.Forms.Label();
            this.lblSmodeCadence = new System.Windows.Forms.Label();
            this.lblSmodeSpeed = new System.Windows.Forms.Label();
            this.buttonConvertUnits = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblFTP = new System.Windows.Forms.Label();
            this.btnChangeFTP = new System.Windows.Forms.Button();
            this.txtFTP = new System.Windows.Forms.TextBox();
            this.lblIntensityFactor = new System.Windows.Forms.Label();
            this.lblTrainingStressScore = new System.Windows.Forms.Label();
            this.lblNormalizedPower = new System.Windows.Forms.Label();
            this.lblTotalDistance = new System.Windows.Forms.Label();
            this.lblAveragePower = new System.Windows.Forms.Label();
            this.lblAverageAltitude = new System.Windows.Forms.Label();
            this.lblAverageHR = new System.Windows.Forms.Label();
            this.lblEntries = new System.Windows.Forms.Label();
            this.lblMaxSpeed = new System.Windows.Forms.Label();
            this.lblMaxAltitude = new System.Windows.Forms.Label();
            this.lblMaxPower = new System.Windows.Forms.Label();
            this.lblMinHR = new System.Windows.Forms.Label();
            this.lblMaxHR = new System.Windows.Forms.Label();
            this.lblAverageSpeed = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblZoomNormalizedPower = new System.Windows.Forms.Label();
            this.lblZoomAverageAltitude = new System.Windows.Forms.Label();
            this.lblZoomMaxAltitude = new System.Windows.Forms.Label();
            this.lblZoomAveragePower = new System.Windows.Forms.Label();
            this.lblZoomMaxPower = new System.Windows.Forms.Label();
            this.lblZoomAverageSpeed = new System.Windows.Forms.Label();
            this.lblZoomMaxSpeed = new System.Windows.Forms.Label();
            this.lblZoomAverageHR = new System.Windows.Forms.Label();
            this.lblZoomMinHR = new System.Windows.Forms.Label();
            this.lblZoomMaxHR = new System.Windows.Forms.Label();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.daySelectedCB = new System.Windows.Forms.ComboBox();
            this.summaryBtn = new System.Windows.Forms.Button();
            this.summaryLBL = new System.Windows.Forms.Label();
            this.btnDetectInterval = new System.Windows.Forms.Button();
            this.intervalListsCB = new System.Windows.Forms.ComboBox();
            this.selectIntBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBoxSmode.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1885, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(120, 26);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(41, 28);
            this.lblDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(42, 17);
            this.lblDate.TabIndex = 1;
            this.lblDate.Text = "Date:";
            // 
            // lblStartTime
            // 
            this.lblStartTime.AutoSize = true;
            this.lblStartTime.Location = new System.Drawing.Point(242, 28);
            this.lblStartTime.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStartTime.Name = "lblStartTime";
            this.lblStartTime.Size = new System.Drawing.Size(77, 17);
            this.lblStartTime.TabIndex = 2;
            this.lblStartTime.Text = "Start Time:";
            // 
            // lblInterval
            // 
            this.lblInterval.AutoSize = true;
            this.lblInterval.Location = new System.Drawing.Point(41, 54);
            this.lblInterval.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblInterval.Name = "lblInterval";
            this.lblInterval.Size = new System.Drawing.Size(58, 17);
            this.lblInterval.TabIndex = 3;
            this.lblInterval.Text = "Interval:";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.time,
            this.interval,
            this.heartrate,
            this.speed,
            this.cadence,
            this.altitude,
            this.power,
            this.powerbalanceindex,
            this.leftlegpower,
            this.rightlegpower,
            this.pedalindex});
            this.dataGridView1.Location = new System.Drawing.Point(44, 88);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(1147, 178);
            this.dataGridView1.TabIndex = 4;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // time
            // 
            this.time.HeaderText = "Time";
            this.time.Name = "time";
            this.time.ReadOnly = true;
            // 
            // interval
            // 
            this.interval.HeaderText = "interval";
            this.interval.Name = "interval";
            this.interval.ReadOnly = true;
            // 
            // heartrate
            // 
            this.heartrate.HeaderText = "Heart Rate (BPM)";
            this.heartrate.Name = "heartrate";
            this.heartrate.ReadOnly = true;
            // 
            // speed
            // 
            this.speed.HeaderText = "Speed (kph)";
            this.speed.Name = "speed";
            this.speed.ReadOnly = true;
            // 
            // cadence
            // 
            this.cadence.HeaderText = "Cadence";
            this.cadence.Name = "cadence";
            this.cadence.ReadOnly = true;
            // 
            // altitude
            // 
            this.altitude.HeaderText = "Altitude (m)";
            this.altitude.Name = "altitude";
            this.altitude.ReadOnly = true;
            // 
            // power
            // 
            this.power.HeaderText = "Power (Watts)";
            this.power.Name = "power";
            this.power.ReadOnly = true;
            // 
            // powerbalanceindex
            // 
            this.powerbalanceindex.HeaderText = "Power Balance and Pedaling Index";
            this.powerbalanceindex.Name = "powerbalanceindex";
            this.powerbalanceindex.ReadOnly = true;
            // 
            // leftlegpower
            // 
            this.leftlegpower.HeaderText = "Left Leg Power";
            this.leftlegpower.Name = "leftlegpower";
            this.leftlegpower.ReadOnly = true;
            // 
            // rightlegpower
            // 
            this.rightlegpower.HeaderText = "Right Leg Power";
            this.rightlegpower.Name = "rightlegpower";
            this.rightlegpower.ReadOnly = true;
            // 
            // pedalindex
            // 
            this.pedalindex.HeaderText = "Pedal Index";
            this.pedalindex.Name = "pedalindex";
            this.pedalindex.ReadOnly = true;
            // 
            // zedGraphControl1
            // 
            this.zedGraphControl1.Location = new System.Drawing.Point(44, 357);
            this.zedGraphControl1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.zedGraphControl1.Name = "zedGraphControl1";
            this.zedGraphControl1.ScrollGrace = 0D;
            this.zedGraphControl1.ScrollMaxX = 0D;
            this.zedGraphControl1.ScrollMaxY = 0D;
            this.zedGraphControl1.ScrollMaxY2 = 0D;
            this.zedGraphControl1.ScrollMinX = 0D;
            this.zedGraphControl1.ScrollMinY = 0D;
            this.zedGraphControl1.ScrollMinY2 = 0D;
            this.zedGraphControl1.Size = new System.Drawing.Size(1147, 506);
            this.zedGraphControl1.TabIndex = 34;
            this.zedGraphControl1.ZoomEvent += new ZedGraph.ZedGraphControl.ZoomEventHandler(this.zedGraphControl1_ZoomEvent);
            this.zedGraphControl1.Load += new System.EventHandler(this.zedGraphControl1_Load);
            // 
            // lblLength
            // 
            this.lblLength.AutoSize = true;
            this.lblLength.Location = new System.Drawing.Point(242, 54);
            this.lblLength.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLength.Name = "lblLength";
            this.lblLength.Size = new System.Drawing.Size(56, 17);
            this.lblLength.TabIndex = 35;
            this.lblLength.Text = "Length:";
            // 
            // groupBoxSmode
            // 
            this.groupBoxSmode.Controls.Add(this.lblSmodeAirPresure);
            this.groupBoxSmode.Controls.Add(this.lblSmodeUnit);
            this.groupBoxSmode.Controls.Add(this.lblSmodeData);
            this.groupBoxSmode.Controls.Add(this.lblSmodePowerPedalingIndex);
            this.groupBoxSmode.Controls.Add(this.lblSmodePowerLRBalance);
            this.groupBoxSmode.Controls.Add(this.lblSmodePower);
            this.groupBoxSmode.Controls.Add(this.lblSmodeAltitude);
            this.groupBoxSmode.Controls.Add(this.lblSmodeCadence);
            this.groupBoxSmode.Controls.Add(this.lblSmodeSpeed);
            this.groupBoxSmode.Location = new System.Drawing.Point(1209, 88);
            this.groupBoxSmode.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBoxSmode.Name = "groupBoxSmode";
            this.groupBoxSmode.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBoxSmode.Size = new System.Drawing.Size(251, 178);
            this.groupBoxSmode.TabIndex = 49;
            this.groupBoxSmode.TabStop = false;
            this.groupBoxSmode.Text = "SMode: ";
            // 
            // lblSmodeAirPresure
            // 
            this.lblSmodeAirPresure.AutoSize = true;
            this.lblSmodeAirPresure.Location = new System.Drawing.Point(7, 146);
            this.lblSmodeAirPresure.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSmodeAirPresure.Name = "lblSmodeAirPresure";
            this.lblSmodeAirPresure.Size = new System.Drawing.Size(121, 17);
            this.lblSmodeAirPresure.TabIndex = 57;
            this.lblSmodeAirPresure.Text = "Air Pressure: OFF";
            // 
            // lblSmodeUnit
            // 
            this.lblSmodeUnit.AutoSize = true;
            this.lblSmodeUnit.Location = new System.Drawing.Point(7, 130);
            this.lblSmodeUnit.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSmodeUnit.Name = "lblSmodeUnit";
            this.lblSmodeUnit.Size = new System.Drawing.Size(156, 17);
            this.lblSmodeUnit.TabIndex = 56;
            this.lblSmodeUnit.Text = "Units: (km, km/h, m, °C)";
            // 
            // lblSmodeData
            // 
            this.lblSmodeData.AutoSize = true;
            this.lblSmodeData.Location = new System.Drawing.Point(7, 114);
            this.lblSmodeData.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSmodeData.Name = "lblSmodeData";
            this.lblSmodeData.Size = new System.Drawing.Size(145, 17);
            this.lblSmodeData.TabIndex = 55;
            this.lblSmodeData.Text = "HR/CC Data: HR Only";
            // 
            // lblSmodePowerPedalingIndex
            // 
            this.lblSmodePowerPedalingIndex.AutoSize = true;
            this.lblSmodePowerPedalingIndex.Location = new System.Drawing.Point(7, 98);
            this.lblSmodePowerPedalingIndex.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSmodePowerPedalingIndex.Name = "lblSmodePowerPedalingIndex";
            this.lblSmodePowerPedalingIndex.Size = new System.Drawing.Size(178, 17);
            this.lblSmodePowerPedalingIndex.TabIndex = 54;
            this.lblSmodePowerPedalingIndex.Text = "Power Pedaling Index: OFF";
            // 
            // lblSmodePowerLRBalance
            // 
            this.lblSmodePowerLRBalance.AutoSize = true;
            this.lblSmodePowerLRBalance.Location = new System.Drawing.Point(7, 82);
            this.lblSmodePowerLRBalance.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSmodePowerLRBalance.Name = "lblSmodePowerLRBalance";
            this.lblSmodePowerLRBalance.Size = new System.Drawing.Size(159, 17);
            this.lblSmodePowerLRBalance.TabIndex = 53;
            this.lblSmodePowerLRBalance.Text = "Power LR Balance: OFF";
            // 
            // lblSmodePower
            // 
            this.lblSmodePower.AutoSize = true;
            this.lblSmodePower.Location = new System.Drawing.Point(7, 66);
            this.lblSmodePower.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSmodePower.Name = "lblSmodePower";
            this.lblSmodePower.Size = new System.Drawing.Size(82, 17);
            this.lblSmodePower.TabIndex = 52;
            this.lblSmodePower.Text = "Power: OFF";
            // 
            // lblSmodeAltitude
            // 
            this.lblSmodeAltitude.AutoSize = true;
            this.lblSmodeAltitude.Location = new System.Drawing.Point(7, 50);
            this.lblSmodeAltitude.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSmodeAltitude.Name = "lblSmodeAltitude";
            this.lblSmodeAltitude.Size = new System.Drawing.Size(90, 17);
            this.lblSmodeAltitude.TabIndex = 51;
            this.lblSmodeAltitude.Text = "Altitude: OFF";
            // 
            // lblSmodeCadence
            // 
            this.lblSmodeCadence.AutoSize = true;
            this.lblSmodeCadence.Location = new System.Drawing.Point(7, 34);
            this.lblSmodeCadence.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSmodeCadence.Name = "lblSmodeCadence";
            this.lblSmodeCadence.Size = new System.Drawing.Size(99, 17);
            this.lblSmodeCadence.TabIndex = 50;
            this.lblSmodeCadence.Text = "Cadence: OFF";
            // 
            // lblSmodeSpeed
            // 
            this.lblSmodeSpeed.AutoSize = true;
            this.lblSmodeSpeed.Location = new System.Drawing.Point(7, 18);
            this.lblSmodeSpeed.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSmodeSpeed.Name = "lblSmodeSpeed";
            this.lblSmodeSpeed.Size = new System.Drawing.Size(84, 17);
            this.lblSmodeSpeed.TabIndex = 49;
            this.lblSmodeSpeed.Text = "Speed: OFF";
            // 
            // buttonConvertUnits
            // 
            this.buttonConvertUnits.Location = new System.Drawing.Point(1466, 156);
            this.buttonConvertUnits.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonConvertUnits.Name = "buttonConvertUnits";
            this.buttonConvertUnits.Size = new System.Drawing.Size(153, 44);
            this.buttonConvertUnits.TabIndex = 59;
            this.buttonConvertUnits.Text = "Convert EURO/US Units";
            this.buttonConvertUnits.UseVisualStyleBackColor = true;
            this.buttonConvertUnits.Click += new System.EventHandler(this.buttonConvertUnits_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblFTP);
            this.groupBox1.Controls.Add(this.btnChangeFTP);
            this.groupBox1.Controls.Add(this.txtFTP);
            this.groupBox1.Controls.Add(this.lblIntensityFactor);
            this.groupBox1.Controls.Add(this.lblTrainingStressScore);
            this.groupBox1.Controls.Add(this.lblNormalizedPower);
            this.groupBox1.Controls.Add(this.lblTotalDistance);
            this.groupBox1.Controls.Add(this.lblAveragePower);
            this.groupBox1.Controls.Add(this.lblAverageAltitude);
            this.groupBox1.Controls.Add(this.lblAverageHR);
            this.groupBox1.Controls.Add(this.lblEntries);
            this.groupBox1.Controls.Add(this.lblMaxSpeed);
            this.groupBox1.Controls.Add(this.lblMaxAltitude);
            this.groupBox1.Controls.Add(this.lblMaxPower);
            this.groupBox1.Controls.Add(this.lblMinHR);
            this.groupBox1.Controls.Add(this.lblMaxHR);
            this.groupBox1.Controls.Add(this.lblAverageSpeed);
            this.groupBox1.Location = new System.Drawing.Point(1198, 270);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(341, 387);
            this.groupBox1.TabIndex = 60;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Statistics";
            // 
            // lblFTP
            // 
            this.lblFTP.AutoSize = true;
            this.lblFTP.Location = new System.Drawing.Point(8, 249);
            this.lblFTP.Name = "lblFTP";
            this.lblFTP.Size = new System.Drawing.Size(165, 17);
            this.lblFTP.TabIndex = 65;
            this.lblFTP.Text = "FTP Value: (Default 310)";
            // 
            // btnChangeFTP
            // 
            this.btnChangeFTP.Location = new System.Drawing.Point(118, 267);
            this.btnChangeFTP.Name = "btnChangeFTP";
            this.btnChangeFTP.Size = new System.Drawing.Size(81, 25);
            this.btnChangeFTP.TabIndex = 63;
            this.btnChangeFTP.Text = "Change";
            this.btnChangeFTP.UseVisualStyleBackColor = true;
            this.btnChangeFTP.Click += new System.EventHandler(this.btnChangeFTP_Click);
            // 
            // txtFTP
            // 
            this.txtFTP.Location = new System.Drawing.Point(11, 269);
            this.txtFTP.Name = "txtFTP";
            this.txtFTP.Size = new System.Drawing.Size(100, 22);
            this.txtFTP.TabIndex = 62;
            // 
            // lblIntensityFactor
            // 
            this.lblIntensityFactor.AutoSize = true;
            this.lblIntensityFactor.Location = new System.Drawing.Point(12, 357);
            this.lblIntensityFactor.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblIntensityFactor.Name = "lblIntensityFactor";
            this.lblIntensityFactor.Size = new System.Drawing.Size(133, 17);
            this.lblIntensityFactor.TabIndex = 61;
            this.lblIntensityFactor.Text = "Intensity Factor (IF):";
            // 
            // lblTrainingStressScore
            // 
            this.lblTrainingStressScore.AutoSize = true;
            this.lblTrainingStressScore.Location = new System.Drawing.Point(9, 338);
            this.lblTrainingStressScore.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTrainingStressScore.Name = "lblTrainingStressScore";
            this.lblTrainingStressScore.Size = new System.Drawing.Size(190, 17);
            this.lblTrainingStressScore.TabIndex = 60;
            this.lblTrainingStressScore.Text = "Training Stress Score (TSS):";
            // 
            // lblNormalizedPower
            // 
            this.lblNormalizedPower.AutoSize = true;
            this.lblNormalizedPower.Location = new System.Drawing.Point(10, 321);
            this.lblNormalizedPower.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNormalizedPower.Name = "lblNormalizedPower";
            this.lblNormalizedPower.Size = new System.Drawing.Size(159, 17);
            this.lblNormalizedPower.TabIndex = 59;
            this.lblNormalizedPower.Text = "Normalized Power (NP):";
            // 
            // lblTotalDistance
            // 
            this.lblTotalDistance.AutoSize = true;
            this.lblTotalDistance.Location = new System.Drawing.Point(7, 23);
            this.lblTotalDistance.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTotalDistance.Name = "lblTotalDistance";
            this.lblTotalDistance.Size = new System.Drawing.Size(103, 17);
            this.lblTotalDistance.TabIndex = 58;
            this.lblTotalDistance.Text = "Total Distance:";
            // 
            // lblAveragePower
            // 
            this.lblAveragePower.AutoSize = true;
            this.lblAveragePower.Location = new System.Drawing.Point(7, 201);
            this.lblAveragePower.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAveragePower.Name = "lblAveragePower";
            this.lblAveragePower.Size = new System.Drawing.Size(108, 17);
            this.lblAveragePower.TabIndex = 57;
            this.lblAveragePower.Text = "Average Power:";
            // 
            // lblAverageAltitude
            // 
            this.lblAverageAltitude.AutoSize = true;
            this.lblAverageAltitude.Location = new System.Drawing.Point(7, 185);
            this.lblAverageAltitude.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAverageAltitude.Name = "lblAverageAltitude";
            this.lblAverageAltitude.Size = new System.Drawing.Size(120, 17);
            this.lblAverageAltitude.TabIndex = 56;
            this.lblAverageAltitude.Text = "Average Altitude: ";
            // 
            // lblAverageHR
            // 
            this.lblAverageHR.AutoSize = true;
            this.lblAverageHR.Location = new System.Drawing.Point(7, 153);
            this.lblAverageHR.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAverageHR.Name = "lblAverageHR";
            this.lblAverageHR.Size = new System.Drawing.Size(89, 17);
            this.lblAverageHR.TabIndex = 55;
            this.lblAverageHR.Text = "Average HR:";
            // 
            // lblEntries
            // 
            this.lblEntries.AutoSize = true;
            this.lblEntries.Location = new System.Drawing.Point(7, 222);
            this.lblEntries.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEntries.Name = "lblEntries";
            this.lblEntries.Size = new System.Drawing.Size(60, 17);
            this.lblEntries.TabIndex = 54;
            this.lblEntries.Text = "Entries: ";
            // 
            // lblMaxSpeed
            // 
            this.lblMaxSpeed.AutoSize = true;
            this.lblMaxSpeed.Location = new System.Drawing.Point(7, 103);
            this.lblMaxSpeed.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMaxSpeed.Name = "lblMaxSpeed";
            this.lblMaxSpeed.Size = new System.Drawing.Size(82, 17);
            this.lblMaxSpeed.TabIndex = 53;
            this.lblMaxSpeed.Text = "Max Speed:";
            // 
            // lblMaxAltitude
            // 
            this.lblMaxAltitude.AutoSize = true;
            this.lblMaxAltitude.Location = new System.Drawing.Point(7, 87);
            this.lblMaxAltitude.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMaxAltitude.Name = "lblMaxAltitude";
            this.lblMaxAltitude.Size = new System.Drawing.Size(88, 17);
            this.lblMaxAltitude.TabIndex = 52;
            this.lblMaxAltitude.Text = "Max Altitude:";
            // 
            // lblMaxPower
            // 
            this.lblMaxPower.AutoSize = true;
            this.lblMaxPower.Location = new System.Drawing.Point(7, 71);
            this.lblMaxPower.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMaxPower.Name = "lblMaxPower";
            this.lblMaxPower.Size = new System.Drawing.Size(84, 17);
            this.lblMaxPower.TabIndex = 51;
            this.lblMaxPower.Text = "Max Power: ";
            // 
            // lblMinHR
            // 
            this.lblMinHR.AutoSize = true;
            this.lblMinHR.Location = new System.Drawing.Point(7, 55);
            this.lblMinHR.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMinHR.Name = "lblMinHR";
            this.lblMinHR.Size = new System.Drawing.Size(58, 17);
            this.lblMinHR.TabIndex = 50;
            this.lblMinHR.Text = "Min HR:";
            // 
            // lblMaxHR
            // 
            this.lblMaxHR.AutoSize = true;
            this.lblMaxHR.Location = new System.Drawing.Point(7, 39);
            this.lblMaxHR.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMaxHR.Name = "lblMaxHR";
            this.lblMaxHR.Size = new System.Drawing.Size(61, 17);
            this.lblMaxHR.TabIndex = 49;
            this.lblMaxHR.Text = "Max HR:";
            // 
            // lblAverageSpeed
            // 
            this.lblAverageSpeed.AutoSize = true;
            this.lblAverageSpeed.Location = new System.Drawing.Point(7, 169);
            this.lblAverageSpeed.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAverageSpeed.Name = "lblAverageSpeed";
            this.lblAverageSpeed.Size = new System.Drawing.Size(110, 17);
            this.lblAverageSpeed.TabIndex = 48;
            this.lblAverageSpeed.Text = "Average Speed:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblZoomNormalizedPower);
            this.groupBox2.Controls.Add(this.lblZoomAverageAltitude);
            this.groupBox2.Controls.Add(this.lblZoomMaxAltitude);
            this.groupBox2.Controls.Add(this.lblZoomAveragePower);
            this.groupBox2.Controls.Add(this.lblZoomMaxPower);
            this.groupBox2.Controls.Add(this.lblZoomAverageSpeed);
            this.groupBox2.Controls.Add(this.lblZoomMaxSpeed);
            this.groupBox2.Controls.Add(this.lblZoomAverageHR);
            this.groupBox2.Controls.Add(this.lblZoomMinHR);
            this.groupBox2.Controls.Add(this.lblZoomMaxHR);
            this.groupBox2.Location = new System.Drawing.Point(1546, 302);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(318, 368);
            this.groupBox2.TabIndex = 70;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Zoom Statistics";
            // 
            // lblZoomNormalizedPower
            // 
            this.lblZoomNormalizedPower.AutoSize = true;
            this.lblZoomNormalizedPower.Location = new System.Drawing.Point(14, 338);
            this.lblZoomNormalizedPower.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblZoomNormalizedPower.Name = "lblZoomNormalizedPower";
            this.lblZoomNormalizedPower.Size = new System.Drawing.Size(130, 17);
            this.lblZoomNormalizedPower.TabIndex = 79;
            this.lblZoomNormalizedPower.Text = "Normalized Power: ";
            // 
            // lblZoomAverageAltitude
            // 
            this.lblZoomAverageAltitude.AutoSize = true;
            this.lblZoomAverageAltitude.Location = new System.Drawing.Point(14, 298);
            this.lblZoomAverageAltitude.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblZoomAverageAltitude.Name = "lblZoomAverageAltitude";
            this.lblZoomAverageAltitude.Size = new System.Drawing.Size(120, 17);
            this.lblZoomAverageAltitude.TabIndex = 78;
            this.lblZoomAverageAltitude.Text = "Average Altitude: ";
            // 
            // lblZoomMaxAltitude
            // 
            this.lblZoomMaxAltitude.AutoSize = true;
            this.lblZoomMaxAltitude.Location = new System.Drawing.Point(14, 270);
            this.lblZoomMaxAltitude.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblZoomMaxAltitude.Name = "lblZoomMaxAltitude";
            this.lblZoomMaxAltitude.Size = new System.Drawing.Size(88, 17);
            this.lblZoomMaxAltitude.TabIndex = 77;
            this.lblZoomMaxAltitude.Text = "Max Altitude:";
            // 
            // lblZoomAveragePower
            // 
            this.lblZoomAveragePower.AutoSize = true;
            this.lblZoomAveragePower.Location = new System.Drawing.Point(14, 233);
            this.lblZoomAveragePower.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblZoomAveragePower.Name = "lblZoomAveragePower";
            this.lblZoomAveragePower.Size = new System.Drawing.Size(112, 17);
            this.lblZoomAveragePower.TabIndex = 76;
            this.lblZoomAveragePower.Text = "Average Power: ";
            // 
            // lblZoomMaxPower
            // 
            this.lblZoomMaxPower.AutoSize = true;
            this.lblZoomMaxPower.Location = new System.Drawing.Point(14, 206);
            this.lblZoomMaxPower.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblZoomMaxPower.Name = "lblZoomMaxPower";
            this.lblZoomMaxPower.Size = new System.Drawing.Size(84, 17);
            this.lblZoomMaxPower.TabIndex = 75;
            this.lblZoomMaxPower.Text = "Max Power: ";
            // 
            // lblZoomAverageSpeed
            // 
            this.lblZoomAverageSpeed.AutoSize = true;
            this.lblZoomAverageSpeed.Location = new System.Drawing.Point(12, 166);
            this.lblZoomAverageSpeed.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblZoomAverageSpeed.Name = "lblZoomAverageSpeed";
            this.lblZoomAverageSpeed.Size = new System.Drawing.Size(114, 17);
            this.lblZoomAverageSpeed.TabIndex = 74;
            this.lblZoomAverageSpeed.Text = "Average Speed: ";
            // 
            // lblZoomMaxSpeed
            // 
            this.lblZoomMaxSpeed.AutoSize = true;
            this.lblZoomMaxSpeed.Location = new System.Drawing.Point(12, 139);
            this.lblZoomMaxSpeed.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblZoomMaxSpeed.Name = "lblZoomMaxSpeed";
            this.lblZoomMaxSpeed.Size = new System.Drawing.Size(86, 17);
            this.lblZoomMaxSpeed.TabIndex = 73;
            this.lblZoomMaxSpeed.Text = "Max Speed: ";
            // 
            // lblZoomAverageHR
            // 
            this.lblZoomAverageHR.AutoSize = true;
            this.lblZoomAverageHR.Location = new System.Drawing.Point(8, 86);
            this.lblZoomAverageHR.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblZoomAverageHR.Name = "lblZoomAverageHR";
            this.lblZoomAverageHR.Size = new System.Drawing.Size(93, 17);
            this.lblZoomAverageHR.TabIndex = 72;
            this.lblZoomAverageHR.Text = "Average HR: ";
            // 
            // lblZoomMinHR
            // 
            this.lblZoomMinHR.AutoSize = true;
            this.lblZoomMinHR.Location = new System.Drawing.Point(8, 59);
            this.lblZoomMinHR.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblZoomMinHR.Name = "lblZoomMinHR";
            this.lblZoomMinHR.Size = new System.Drawing.Size(62, 17);
            this.lblZoomMinHR.TabIndex = 71;
            this.lblZoomMinHR.Text = "Min HR: ";
            // 
            // lblZoomMaxHR
            // 
            this.lblZoomMaxHR.AutoSize = true;
            this.lblZoomMaxHR.Location = new System.Drawing.Point(8, 32);
            this.lblZoomMaxHR.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblZoomMaxHR.Name = "lblZoomMaxHR";
            this.lblZoomMaxHR.Size = new System.Drawing.Size(65, 17);
            this.lblZoomMaxHR.TabIndex = 70;
            this.lblZoomMaxHR.Text = "Max HR: ";
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(15, 26);
            this.monthCalendar1.Margin = new System.Windows.Forms.Padding(12, 11, 12, 11);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 71;
            this.monthCalendar1.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar1_DateChanged);
            // 
            // daySelectedCB
            // 
            this.daySelectedCB.FormattingEnabled = true;
            this.daySelectedCB.Location = new System.Drawing.Point(15, 251);
            this.daySelectedCB.Margin = new System.Windows.Forms.Padding(4);
            this.daySelectedCB.Name = "daySelectedCB";
            this.daySelectedCB.Size = new System.Drawing.Size(160, 24);
            this.daySelectedCB.TabIndex = 72;
            this.daySelectedCB.SelectedIndexChanged += new System.EventHandler(this.daySelectedCB_SelectedIndexChanged);
            // 
            // summaryBtn
            // 
            this.summaryBtn.Location = new System.Drawing.Point(183, 248);
            this.summaryBtn.Margin = new System.Windows.Forms.Padding(4);
            this.summaryBtn.Name = "summaryBtn";
            this.summaryBtn.Size = new System.Drawing.Size(100, 28);
            this.summaryBtn.TabIndex = 73;
            this.summaryBtn.Text = "Open File";
            this.summaryBtn.UseVisualStyleBackColor = true;
            this.summaryBtn.Click += new System.EventHandler(this.summaryBtn_Click);
            // 
            // summaryLBL
            // 
            this.summaryLBL.AutoSize = true;
            this.summaryLBL.Location = new System.Drawing.Point(365, 26);
            this.summaryLBL.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.summaryLBL.Name = "summaryLBL";
            this.summaryLBL.Size = new System.Drawing.Size(116, 17);
            this.summaryLBL.TabIndex = 74;
            this.summaryLBL.Text = "Calendar Details:";
            // 
            // btnDetectInterval
            // 
            this.btnDetectInterval.Location = new System.Drawing.Point(6, 33);
            this.btnDetectInterval.Name = "btnDetectInterval";
            this.btnDetectInterval.Size = new System.Drawing.Size(103, 60);
            this.btnDetectInterval.TabIndex = 76;
            this.btnDetectInterval.Text = "Detect Intervals";
            this.btnDetectInterval.UseVisualStyleBackColor = true;
            this.btnDetectInterval.Click += new System.EventHandler(this.btnDetectInterval_Click);
            // 
            // intervalListsCB
            // 
            this.intervalListsCB.FormattingEnabled = true;
            this.intervalListsCB.Location = new System.Drawing.Point(218, 57);
            this.intervalListsCB.Name = "intervalListsCB";
            this.intervalListsCB.Size = new System.Drawing.Size(213, 24);
            this.intervalListsCB.TabIndex = 78;
            // 
            // selectIntBtn
            // 
            this.selectIntBtn.Location = new System.Drawing.Point(451, 44);
            this.selectIntBtn.Name = "selectIntBtn";
            this.selectIntBtn.Size = new System.Drawing.Size(123, 46);
            this.selectIntBtn.TabIndex = 79;
            this.selectIntBtn.Text = "View Interval Stats";
            this.selectIntBtn.UseVisualStyleBackColor = true;
            this.selectIntBtn.Click += new System.EventHandler(this.selectIntBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(115, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 17);
            this.label1.TabIndex = 80;
            this.label1.Text = "Select Interval:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnDetectInterval);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.intervalListsCB);
            this.groupBox3.Controls.Add(this.selectIntBtn);
            this.groupBox3.Location = new System.Drawing.Point(245, 872);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(590, 113);
            this.groupBox3.TabIndex = 81;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Intervals";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.monthCalendar1);
            this.groupBox4.Controls.Add(this.daySelectedCB);
            this.groupBox4.Controls.Add(this.summaryLBL);
            this.groupBox4.Controls.Add(this.summaryBtn);
            this.groupBox4.Location = new System.Drawing.Point(1199, 676);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(531, 309);
            this.groupBox4.TabIndex = 82;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Calender View";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1885, 997);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonConvertUnits);
            this.Controls.Add(this.groupBoxSmode);
            this.Controls.Add(this.lblLength);
            this.Controls.Add(this.zedGraphControl1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.lblInterval);
            this.Controls.Add(this.lblStartTime);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBoxSmode.ResumeLayout(false);
            this.groupBoxSmode.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblStartTime;
        private System.Windows.Forms.Label lblInterval;
        private System.Windows.Forms.DataGridView dataGridView1;
        private ZedGraph.ZedGraphControl zedGraphControl1;
        private System.Windows.Forms.Label lblLength;
        private System.Windows.Forms.GroupBox groupBoxSmode;
        public System.Windows.Forms.Label lblSmodeSpeed;
        public System.Windows.Forms.Label lblSmodeAirPresure;
        public System.Windows.Forms.Label lblSmodeUnit;
        public System.Windows.Forms.Label lblSmodeData;
        public System.Windows.Forms.Label lblSmodePowerPedalingIndex;
        public System.Windows.Forms.Label lblSmodePowerLRBalance;
        public System.Windows.Forms.Label lblSmodePower;
        public System.Windows.Forms.Label lblSmodeAltitude;
        public System.Windows.Forms.Label lblSmodeCadence;
        private System.Windows.Forms.Button buttonConvertUnits;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblTotalDistance;
        private System.Windows.Forms.Label lblAveragePower;
        private System.Windows.Forms.Label lblAverageAltitude;
        private System.Windows.Forms.Label lblAverageHR;
        private System.Windows.Forms.Label lblEntries;
        private System.Windows.Forms.Label lblMaxSpeed;
        private System.Windows.Forms.Label lblMaxAltitude;
        private System.Windows.Forms.Label lblMaxPower;
        private System.Windows.Forms.Label lblMinHR;
        private System.Windows.Forms.Label lblMaxHR;
        private System.Windows.Forms.Label lblAverageSpeed;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblZoomAverageAltitude;
        private System.Windows.Forms.Label lblZoomMaxAltitude;
        private System.Windows.Forms.Label lblZoomAveragePower;
        private System.Windows.Forms.Label lblZoomMaxPower;
        private System.Windows.Forms.Label lblZoomAverageSpeed;
        private System.Windows.Forms.Label lblZoomMaxSpeed;
        private System.Windows.Forms.Label lblZoomAverageHR;
        private System.Windows.Forms.Label lblZoomMinHR;
        private System.Windows.Forms.Label lblZoomMaxHR;
        private System.Windows.Forms.Label lblNormalizedPower;
        private System.Windows.Forms.Label lblIntensityFactor;
        private System.Windows.Forms.Label lblTrainingStressScore;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.ComboBox daySelectedCB;
        private System.Windows.Forms.Button summaryBtn;
        private System.Windows.Forms.Label summaryLBL;
        private System.Windows.Forms.Button btnDetectInterval;
        private System.Windows.Forms.ComboBox intervalListsCB;
        private System.Windows.Forms.Button selectIntBtn;
        private System.Windows.Forms.DataGridViewTextBoxColumn time;
        private System.Windows.Forms.DataGridViewTextBoxColumn interval;
        private System.Windows.Forms.DataGridViewTextBoxColumn heartrate;
        private System.Windows.Forms.DataGridViewTextBoxColumn speed;
        private System.Windows.Forms.DataGridViewTextBoxColumn cadence;
        private System.Windows.Forms.DataGridViewTextBoxColumn altitude;
        private System.Windows.Forms.DataGridViewTextBoxColumn power;
        private System.Windows.Forms.DataGridViewTextBoxColumn powerbalanceindex;
        private System.Windows.Forms.DataGridViewTextBoxColumn leftlegpower;
        private System.Windows.Forms.DataGridViewTextBoxColumn rightlegpower;
        private System.Windows.Forms.DataGridViewTextBoxColumn pedalindex;
        private System.Windows.Forms.Label lblZoomNormalizedPower;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label lblFTP;
        private System.Windows.Forms.Button btnChangeFTP;
        private System.Windows.Forms.TextBox txtFTP;
    }
}

