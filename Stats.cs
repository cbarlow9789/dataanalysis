﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolarData
{
    class Stats
    {

        public int TotalRows = 0;

        public int RowCount = 0;
        public int RowCount2 = 0;

        public double TotalHR;
        public double HR;
        public double MaxHR = Int32.MinValue;
        public double MinHR = Int32.MaxValue;
        public double AverageHR;

        public double TotalPower;
        public double TotalPower2;
        public int Power;
        public double MaxPower = Int32.MinValue;
        public double AveragePower;
        public double AveragePower2;

        public double NormalizedPower;

        public double TotalAltitude;
        public int Altitude;
        public double MaxAltitude = Int32.MinValue;
        public double AverageAltitude;

        public double TotalSpeed;
        public double TotalDistance;
        public double Speed;
        public double MaxSpeed = Int32.MinValue;
        public double AverageSpeed;

        public int PowerBalanceValue;
        public int LeftPower;
        public int RightPower;
        public int PedalIndex;

        public List<double> RollingAverageList = new List<double>();

        double RollingAverageTotal = 0;
        double NormalizedPowerAverage = 0;

        public double FunctionalThresholdPower = 300;
        public double IntensityFactor;
        public double TrainingStressScore;

        public int Interval;

        public void resetStats()
        {
        RowCount = 0;
        RowCount2 = 0;

        TotalHR = 0;
        HR = 0;
        MaxHR = Int32.MinValue;
        MinHR = Int32.MaxValue;
        AverageHR = 0;

        TotalPower = 0;
        Power = 0;
        MaxPower = Int32.MinValue;
        AveragePower = 0;

        TotalAltitude = 0;
        Altitude = 0;
        MaxAltitude = Int32.MinValue;
        AverageAltitude = 0;

        TotalSpeed = 0;
        TotalDistance = 0;
        Speed = 0;
        MaxSpeed = Int32.MinValue;
        AverageSpeed = 0;

        PowerBalanceValue = 0;
        LeftPower = 0;
        RightPower = 0;
        PedalIndex = 0;

        IntensityFactor = 0;
        TrainingStressScore = 0;

        RollingAverageTotal = 0;
        NormalizedPowerAverage = 0;
        NormalizedPower = 0;

        RollingAverageList.Clear();

        AveragePower2 = 0;
        TotalPower2 = 0;

        }


        public void maxHR(double HR)        //Calculates current maximum heart rate
        {
            if (HR > MaxHR)
            {
                MaxHR = HR;
            }
        }

        public void minHR(double HR)        //Calculates minimum heart rate
        {
            if (HR < MinHR)
            {
                MinHR = HR;
            }
        }

        public void maxPower(int Power)     //Calculates Maximum Power
        {
            if (Power > MaxPower)
            {
                MaxPower = Power;
            }
        }

        public void maxAltitude(int Altitude)       //Calculates maximum altitude
        {
            if (Altitude > MaxAltitude)
            {
                MaxAltitude = Altitude;
            }
        }

        public void maxSpeed(double Speed)      //Calculates maximum speed
        {
            if (Speed > MaxSpeed)
            {
                MaxSpeed = Speed;
            }
        }

        public void averageSpeed(double Speed)      //Calculates average speed
        {
            TotalSpeed = TotalSpeed + Speed;
            AverageSpeed = TotalSpeed / TotalRows;
        }

        public void averageHR(double HR)        //Calculates average heart rate
        {
            TotalHR = TotalHR + HR;
            AverageHR = TotalHR / RowCount;
        }

        public void averagePower()      //Calculates average power
        {
            AveragePower = TotalPower / RowCount;
        }

        public void normalizedPowerValues(double Power)      //Calculates average power every 30 seconds
        {
           
            if (RowCount2 != 0)
            {
                if (RowCount2 * Interval % 30 == 0)
                {
                    AveragePower2 = TotalPower2 / RowCount2;
                    RollingAverageList.Add(AveragePower2);
                    TotalPower2 = 0;
                    RowCount2 = 0;
                }
            }
 
        }

        public void normalizedPower()
        {
            
            for (int i = 0; i < RollingAverageList.Count; i++)
            {
                RollingAverageList[i] = Math.Pow(RollingAverageList[i], 4);
            }

            for (int i = 0; i < RollingAverageList.Count; i++)
            {
                RollingAverageTotal = RollingAverageTotal + RollingAverageList[i];
                NormalizedPowerAverage = RollingAverageTotal / RollingAverageList.Count();
            }

            NormalizedPower = Math.Pow(NormalizedPowerAverage, 0.25);
        }

        public void averageAltitude(double Altitude)        //Calculates average altitude
        {
            TotalAltitude = TotalAltitude + Altitude;
            AverageAltitude = TotalAltitude / RowCount;
        }

        public void totalDistance(double timeSeconds)       //Calculates total distance
        {
            TotalDistance= (timeSeconds/3600) * AverageSpeed;
        }

        public void LRPowerBalancePedalIndex(int powerBalance)
        {
            LeftPower = PowerBalanceValue & 255;
            RightPower = 100 - LeftPower;

            PedalIndex = (PowerBalanceValue & 65280) >> 8;

        }

        public void trainingStressScore()
        {
            IntensityFactor = (NormalizedPower / FunctionalThresholdPower);
            TrainingStressScore = (RowCount * NormalizedPower * IntensityFactor ) /  (FunctionalThresholdPower * 3600) * 100; 
        
        }

    }
}
